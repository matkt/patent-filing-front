# Patent Filing Dapp Web

A demo application that lets you manage patents in a decentralized way
This project uses several concepts (Event Driven Architecture, Domain Driven Design, Microservices and Smart contract)

* Front submodule

## Technologies used
 * reactjs
 * redux
 * web3j

## Getting Started

Patent filing dapp web repository contains :

* decentralized web application to interact with the smart contract

### Installing

Download dependencies

```
npm install
```

## Running the decentralized web application

```
npm run start
```

Then visit :  http://localhost:3000/

Karim Taam
