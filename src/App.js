import React, { Component } from 'react';
import logo from './img/logo.svg';
import './App.css';

//contract
import PatentFilingServiceArtifacts from './contracts/PatentFiling.json'
import { default as contract } from 'truffle-contract';
import getWeb3 from './utils/getWeb3'

//pages
import {Redirect, Route, NavLink, HashRouter} from "react-router-dom";
import Settings from "./pages/Settings";
import AddPatent from "./pages/AddPatent";
import ViewPatents from "./pages/ViewPatents";

//redux
import { connect } from 'react-redux';
import { initWeb3Action } from './actions/ContractAction'

let PatentFilingService = contract(PatentFilingServiceArtifacts)

class App extends Component {

  constructor(props) {
      super(props)
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.

    getWeb3
    .then(results => {
      this.props.initWeb3Action(results.web3)
    })
    .catch(() => {
      console.log('Error finding web3.')
    })
  }

  render() {
    return (
      <div className="App">
        { this.renderHeader() }
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <div className="row">
                <HashRouter>
                  <div className="page-content">
                    <ul className="nav nav-tabs" role="tablist">
                      <li className="nav-item">
                        <NavLink className="nav-link" data-toggle="tab" role="tab" aria-controls="ViewPatents" aria-selected="true" to="/ViewPatents">View Patents</NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink className="nav-link" data-toggle="tab" role="tab" aria-controls="AddPatent" aria-selected="false" to="/AddPatent">Add Patent</NavLink>
                      </li>
                      <li className="nav-item">
                        <NavLink className="nav-link" data-toggle="tab" role="tab" aria-controls="Contact" aria-selected="false" to="/Settings">Settings</NavLink>
                      </li>
                    </ul>
                    <div className="content">
                      <Redirect from="/" to="ViewPatents" />
                      <Route exact path="/ViewPatents" component={ViewPatents}/>
                      <Route exact path="/AddPatent" component={AddPatent}/>
                      <Route exact path="/Settings" component={Settings}/>
                    </div>
                  </div>
                </HashRouter>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderHeader = () => {
    return <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <h1>Patent Filing dapp web</h1>

    </header>
  }

  /**** COMMON ACTION ****/

  handleSubmit(event) {
    event.preventDefault();
  }


}

const mapStateToProps = (state) => {
  return { web3: state.contractReducer.web3}
};

const mapDispatchToProps = dispatch => ({
  initWeb3Action: (web3) => dispatch(initWeb3Action(web3))
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
