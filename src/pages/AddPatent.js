import React, { Component } from "react";
import { connect } from 'react-redux';

import PatentFilingServiceArtifacts from '../contracts/PatentFiling.json'
import { default as contract } from 'truffle-contract';
import getWeb3 from '../utils/getWeb3'

let PatentFilingService = contract(PatentFilingServiceArtifacts)

class AddPatent extends Component {


  constructor(props) {
      super(props)

      this.state = {
        inputName: '',
        inputDescription: '',
        inputFile: null
      }

      this.handleSubmit = this.handleSubmit.bind(this);
  }

  render() {
    return (
      <form>
        <div className="form-inline form-group">
          <label htmlFor="inputName">Name</label>
          <input type="text" className="form-control" name="inputName" maxLength="50" placeholder="Name"
              value={this.state.inputName} onChange={this.handleChange.bind(this)}/>
        </div>
        <div className="form-inline form-group">
          <label htmlFor="inputDescription">Description</label>
          <textarea name="inputDescription" className="form-control" cols="100" rows="5" maxLength="200" placeholder="Description"
              value={this.state.inputDescription} onChange={this.handleChange.bind(this)}></textarea>
        </div>
        <div className="form-inline form-group">
          <label htmlFor="inputFile">File input</label>
          <input type="file" name="inputFile" ref={(ref) => this.fileUpload = ref} />
        </div>
        <button type="submit" onClick={this.registerPatentInBackend.bind(this)}  className="btn btn-primary">Submit</button>
      </form>
    );
  }

  /**** COMMON ACTION ****/

  handleSubmit(event) {
    event.preventDefault();
  }

  /****** HISTORICAL POSITION ACTION *******/

  handleChange(event) {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value
    })
  }

  registerPatentInBackend(event) {
    event.preventDefault()
    //	fr.readAsArrayBuffer(this.state.inputFile);
    var name = this.state.inputName;
    var description = this.state.inputDescription;
    var file = this.fileUpload.files[0];
    var address = this.props.backendAddress+':8090/addPatent';

    var reader = new FileReader();
    if (reader) {
      reader.onloadend = () => {
          const buffer = Buffer.from(reader.result)
          fetch(address, {
            method: 'post',
            mode: 'cors',
            headers: {'Access-Control-Allow-Origin':'*', "Content-Type": "application/json",},
            body: "{\"name\":\""+name+"\",  \"description\":\""+description+"\", \"content\":\""+buf2hex(buffer)+"\"}"
          }).then(response => {
            return response.json()
          })
          .then(body => {
            console.log("Response from backend received : ", body.ipfs)
            this.registerPatentInBlockchain(body.ipfs)
          });
      }
      reader.readAsArrayBuffer(file)
    }
    return false
  }

  registerPatentInBlockchain(ipfs) {
    PatentFilingService.setProvider(this.props.web3.currentProvider);
    var patentFilingInstance;
    this.props.web3.eth.getAccounts((error, accounts) => {
      PatentFilingService.at(this.props.contractAddress).then((instance) => {
        patentFilingInstance = instance
        console.log("IPFS input : ", ipfs);
        return patentFilingInstance.recordPatent(
            ipfs,
            {from: accounts[0]}
        ).then((result) => {
            console.log("recordPatent : ", result);
            alert("Patent recorded")
        })
      })
    })
  }
}


function buf2hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

const mapStateToProps = (state) => {
  return {
    contractAddress: state.contractReducer.contractAddress,
    backendAddress: state.backendReducer.backendAddress,
    web3: state.contractReducer.web3
  }
};


export default connect(mapStateToProps)(AddPatent);
