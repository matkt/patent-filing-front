import React, { Component } from "react";
import { connect } from 'react-redux';
import { changeContractAddressAction  } from '../actions/ContractAction'
import { changeBackendAddressAction  } from '../actions/BackendAction'

class Settings extends Component  {

  render() {
     console.log('another view trigger contract:', this.props.contractAddress);
    return (
      <div>
          <div className="form-group form-inline">
            <label htmlFor="contractAddress">Contract address &nbsp;</label>
            <input type="text" className="form-control" name="contractAddress" value={this.props.contractAddress}
              onChange={this.handleClickChangeContractAddress.bind(this)}/>
          </div>
          <div className="form-group form-inline">
            <label htmlFor="backendAddress">IP Server &nbsp;</label>
            <input type="text" className="form-control" name="backendAddress" value={this.props.backendAddress}
              onChange={this.handleClickChangeBackendAddress.bind(this)}/>
          </div>
      </div>
    );
  }

  handleClickChangeContractAddress(event) {
    this.props.changeContractAddressAction(event.target.value)
  }

  handleClickChangeBackendAddress(event) {
    this.props.changeBackendAddressAction(event.target.value)
  }

}


const mapStateToProps = (state) => {
  return {
    contractAddress: state.contractReducer.contractAddress,
    backendAddress: state.backendReducer.backendAddress
  }
};

const mapDispatchToProps = dispatch => ({
  changeContractAddressAction: (newAddress) => dispatch(changeContractAddressAction(newAddress)),
  changeBackendAddressAction: (newAddress) => dispatch(changeBackendAddressAction(newAddress))
})

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
