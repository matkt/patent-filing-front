import React, { Component } from "react";
import { connect } from 'react-redux';

import IconButton from '@material-ui/core/IconButton';
import CheckCircle from '@material-ui/icons/CheckCircle';

class ViewPatents extends Component {

  constructor(props) {
    super(props);

    this.state = {
      listPatents : null
    }

  }

  componentWillMount() {
    fetch(this.props.backendAddress+':8090/getAllPatents',{
      mode: 'cors',headers: {'Access-Control-Allow-Origin':'*'}
    })
    .then(response => {
      return response.json()
    })
    .then(body => {
      this.setState({ listPatents: body})}
    );
  }

  render() {
    return (
      <div className="container">
        <h2>&nbsp; A demo application that lets you manage patents in a decentralized way.</h2><br/>
        { this.renderTableAllPatents() }
      </div>
    );
  }

  renderTableAllPatents = () => {
    var context = this;
    var listPatents =  context.state.listPatents;
    if(listPatents!=null){
      return <table className="table">
              <thead className="thead-light">
                <tr>
                  <th scope="col">IPFS</th>
                  <th scope="col">Description</th>
                  <th scope="col">Record transaction hash</th>
                  <th scope="col">Approved transaction hash</th>
                </tr>
              </thead>
              <tbody>
                {
                    listPatents.map(function(patent, index){
                    var link = context.props.backendAddress+":8080/ipfs/"+patent.ipfs
                    return <tr key={index}>
                               <td><a href={link}>{patent.ipfs}</a></td>
                               <td><div className="textMultiline">{patent.patentDescription}</div></td>
                               <td>
                                  <IconButton value={patent.recordedTransactionHash} aria-label="RecordedTransactionHash" onClick={context.displayHashTrx.bind(context)}>
                                    <CheckCircle color={patent.recordedTransactionHash!=null?"primary":"error"}/>
                                  </IconButton>
                               </td>
                               <td>
                                  <IconButton value={patent.approvedTransactionHash} aria-label="RecordedTransactionHash" onClick={context.displayHashTrx.bind(context)}>
                                    <CheckCircle color={patent.approvedTransactionHash!=null?"primary":"error"}/>
                                  </IconButton>
                               </td>
                            </tr>;
                   })
                }
              </tbody>
            </table>
    }
  }

  displayHashTrx(event) {
    if(event.target.value!=""){
       alert(event.target.value);
    }
  }

}


const mapStateToProps = (state) => {
  return {
    contractAddress: state.contractReducer.contractAddress,
    backendAddress: state.backendReducer.backendAddress
  }
};


export default connect(mapStateToProps)(ViewPatents);
