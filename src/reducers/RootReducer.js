import { combineReducers } from 'redux';
import contractReducer from './ContractReducer';
import backendReducer from './BackendReducer';

export default combineReducers({
 contractReducer, backendReducer
});
