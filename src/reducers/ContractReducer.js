export default (state = {
  contractAddress:"0x9c253d6f4a0b9269aece386936372236bae10007",
  web3:null
}, action) => {
 switch (action.type) {
  case 'CHANGE_CONTRACT_ADDRESS':
   console.log('change contract address action : ', action);
   return {
      contractAddress: action.payload,
      web3 : state.web3
   }
  case 'INIT_WEB3':
   console.log('init web3 action : ', action);
   return {
      contractAddress: state.contractAddress,
      web3: action.payload
   }
  default:
   return state
 }
}
