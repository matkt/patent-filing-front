export default (state = {
  backendAddress:(process.env.REACT_APP_HOST == undefined)?("http://"+window.location.hostname):(process.env.REACT_APP_HOST.replace(" ",""))
}, action) => {
 switch (action.type) {
  case 'CHANGE_BACKEND_ADDRESS':
   console.log('change backend address action : ', action);
   return {
      backendAddress: action.payload
   }
  default:
   return state
 }
}
