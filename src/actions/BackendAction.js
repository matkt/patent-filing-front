export const changeBackendAddressAction = (newAddress) => dispatch => {
 dispatch({
  type: 'CHANGE_BACKEND_ADDRESS',
  payload: newAddress
 })
}
