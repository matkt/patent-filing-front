export const changeContractAddressAction = (newAddress) => dispatch => {
 dispatch({
  type: 'CHANGE_CONTRACT_ADDRESS',
  payload: newAddress
 })
}

export const initWeb3Action = (web3) => dispatch => {
 dispatch({
  type: 'INIT_WEB3',
  payload: web3
 })
}
